package org.ultramine.permission;

import org.ultramine.core.permissions.Permissions;
import org.ultramine.core.service.InjectService;
import org.ultramine.permission.IPermissionManager;

import javax.annotation.Nonnull;

public class PermissionsService implements Permissions
{
	@InjectService
	private static IPermissionManager mgr;

	public PermissionsService()
	{

	}

	@Override
	public boolean has(String world, String player, String permission)
	{
		return mgr.has(world, player, permission);
	}

	@Override
	public @Nonnull String getMeta(String world, String player, String meta)
	{
		return mgr.getMeta(world, player, meta);
	}
}
