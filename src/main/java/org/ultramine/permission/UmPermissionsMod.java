package org.ultramine.permission;

import cpw.mods.fml.common.Mod;
import cpw.mods.fml.common.event.FMLInitializationEvent;

import cpw.mods.fml.common.event.FMLServerStartedEvent;
import cpw.mods.fml.common.event.FMLServerStartingEvent;
import org.ultramine.core.permissions.Permissions;
import org.ultramine.core.service.InjectService;
import org.ultramine.core.service.ServiceManager;
import org.ultramine.permission.IPermissionManager;
import org.ultramine.permission.PermissionRepository;
import org.ultramine.permission.commands.BasicPermissionCommands;
import org.ultramine.permission.internal.ServerPermissionManager;
import org.ultramine.server.ConfigurationHandler;

@Mod(modid = "UM-Permissions", name = "UltraMine Permissions", version = "@version@", acceptableRemoteVersions = "*")
public class UmPermissionsMod
{
	@InjectService
	private static ServiceManager services;
	@InjectService
	private static IPermissionManager perms;

	@Mod.EventHandler
	public void init(FMLInitializationEvent e)
	{

	}

	@Mod.EventHandler
	public void serverStarting(FMLServerStartingEvent e)
	{
		IPermissionManager mgr = new ServerPermissionManager(ConfigurationHandler.getSettingDir(), new PermissionRepository());
		services.register(IPermissionManager.class, mgr, 0);
		services.register(Permissions.class, new PermissionsService(), 1000);
		e.registerCommands(BasicPermissionCommands.class);
	}

	@Mod.EventHandler
	public void serverStarted(FMLServerStartedEvent e)
	{
		perms.reload();
	}
}
